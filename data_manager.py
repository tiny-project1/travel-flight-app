import requests

from constant import SHEET_ENDPOINT_URL
from flight_search import FlightSearch


class DataManager:
    """Manage the data from flight, it use sheety api."""

    def __init__(self):
        self.destination_data = {}

    def get_destination_data(self):
        """Get prices destination from google sheet

        Returns:
            _type_: _description_
        """
        sheet_response = requests.get(url=SHEET_ENDPOINT_URL)
        data = sheet_response.json()
        self.destination_data = data["prices"]

        return self.destination_data

    def update_destination_codes(self):
        """Get airport code"""
        flight_search = FlightSearch()
        for row in self.destination_data:
            sheet_inputs = {"price": {"iataCode": row["iataCode"]}}
            sheet_response = requests.put(
                url=f"{SHEET_ENDPOINT_URL}/{row['id']}", json=sheet_inputs
            )
