import smtplib

from constant import EMAIL, MY_EMAIL, PASSWORD

MY_EMAIL = MY_EMAIL
PASSWORD = PASSWORD


class NotificationManager:
    def __init__(self):
        self.mail_server = "smtp.gmail.com"
        self.port = 587
        self.timeout = 120
        self.email = MY_EMAIL
        self.addrs = EMAIL
        self.password = PASSWORD

    def send_email(self, message):
        connection = smtplib.SMTP(
            self.mail_server, port=self.port, timeout=self.timeout
        )
        connection.starttls()
        connection.login(user=self.email, password=self.password)
        connection.sendmail(
            from_addr=self.email,
            to_addrs=self.addrs,
            msg=f"Subject :cheap flight\n\n{message}",
        )
        connection.close()
