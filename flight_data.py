class FlightData:
    """Store data fligh in class to be easier to access"""

    def __init__(
        self,
        price: int,
        origin_city: str,
        origin_airport: str,
        destination_city: str,
        destination_airport,
        out_date,
        return_date,
    ):
        self.price = price
        self.origin_city = origin_city
        self.origin_airport = origin_airport
        self.destination_city = destination_city
        self.destination_airport = destination_airport
        self.out_date = out_date
        self.return_date = return_date
