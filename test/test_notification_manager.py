from unittest.mock import patch

from notification_manager import NotificationManager

MY_EMAIL = "my.email@gmail.com"
PASSWORD = "mypassword"
EMAIL = "destination.email@gmail.com"


@patch("smtplib.SMTP")
def test_send_email(mock_smtp):
    manager = NotificationManager()
    manager.email = MY_EMAIL
    manager.addrs = EMAIL
    message = "This is a test message"
    manager.send_email(message)

    mock_smtp.assert_called_once_with(
        manager.mail_server, port=manager.port, timeout=manager.timeout
    )
    mock_smtp.return_value.starttls.assert_called_once()
    mock_smtp.return_value.login.assert_called_once_with(
        user=manager.email, password=manager.password
    )
    mock_smtp.return_value.sendmail.assert_called_once_with(
        from_addr=MY_EMAIL,
        to_addrs=EMAIL,
        msg=f"Subject :cheap flight\n\n{message}",
    )
    mock_smtp.return_value.close.assert_called_once()
