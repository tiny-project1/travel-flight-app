import unittest
from unittest.mock import Mock, patch

from data_manager import DataManager


class TestDataManager(unittest.TestCase):
    @patch("data_manager.requests.get")
    def test_get_destination_data(self, mock_requests_get):
        dm = DataManager()
        mock_response = Mock()
        mock_response.json.return_value = {
            "prices": [
                {"city": "Paris", "iataCode": "PAR",
                 "lowestPrice": 100, "id": "1"},
                {"city": "New York", "iataCode": "NYC",
                 "lowestPrice": 200, "id": "2"},
            ]
        }
        mock_requests_get.return_value = mock_response
        expected_result = [
            {"city": "Paris", "iataCode": "PAR",
             "lowestPrice": 100, "id": "1"},
            {"city": "New York", "iataCode": "NYC",
             "lowestPrice": 200, "id": "2"},
        ]
        result = dm.get_destination_data()
        self.assertEqual(result, expected_result)
