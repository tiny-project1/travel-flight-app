from datetime import datetime
from unittest.mock import Mock, patch

from flight_data import FlightData
from flight_search import FlightSearch


def test_get_destination_code(monkeypatch):
    response_mock = Mock()
    response_mock.json.return_value = {"locations": [{"code": "PAR"}]}
    monkeypatch.setattr("requests.get", lambda *args, **kwargs: response_mock)
    fs = FlightSearch()
    city_name = "Paris"
    code = fs.get_destination_code(city_name)
    assert code == "PAR"


@patch("flight_search.requests.get")
def test_search_flight(mock_get):
    mock_json_data = {
        "data": [
            {
                "price": 123.45,
                "route": [
                    {
                        "cityFrom": "London",
                        "flyFrom": "LHR",
                        "cityTo": "Paris",
                        "flyTo": "CDG",
                        "local_departure": "2022-05-10T10:00:00Z",
                    },
                    {
                        "cityFrom": "Paris",
                        "flyFrom": "CDG",
                        "cityTo": "London",
                        "flyTo": "LHR",
                        "local_departure": "2022-05-15T12:00:00Z",
                    },
                ],
            }
        ]
    }
    mock_response = mock_get.return_value
    mock_response.json.return_value = mock_json_data

    fs = FlightSearch()
    from_time = datetime.strptime("2022-05-10", "%Y-%m-%d")
    to_time = datetime.strptime("2022-05-15", "%Y-%m-%d")
    flight_data = fs.search_filght("LON", "PAR", from_time, to_time)
    assert isinstance(flight_data, FlightData)
    assert flight_data is not None
    assert flight_data.price == 123.45
    assert flight_data.origin_city == "London"
    assert flight_data.origin_airport == "LHR"
    assert flight_data.destination_city == "Paris"
    assert flight_data.destination_airport == "CDG"
    assert flight_data.out_date == "2022-05-10"
    assert flight_data.return_date == "2022-05-15"
