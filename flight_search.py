# PYTHONIOENCODING=utf8

import requests

from constant import ENDPOINT_URL, apikey
from flight_data import FlightData

headers = {
    "apikey": apikey,
}


class FlightSearch:
    def get_destination_code(self, city_name):
        parameters = {
            "term": city_name,
            "location_types": "city",
        }

        response = requests.get(
            url=f"{ENDPOINT_URL}/locations/query",
            params=parameters,
            headers=headers
        )
        code = response.json()["locations"][0]["code"]
        return code

    def search_filght(
        self, origin_city_code, destination_city_code, from_time, to_time
    ):
        parameters = {
            "fly_from": origin_city_code,
            "fly_to": destination_city_code,
            "date_from": from_time.strftime("%d/%m/%Y"),
            "date_to": to_time.strftime("%d/%m/%Y"),
            "curr": "EUR",
            "nights_in_dst_from": 4,
            "nights_in_dst_to": 28,
            "flight_type": "round",
            "limit": 1,
            "max_stopovers": 0,
        }

        response = requests.get(
            url=f"{ENDPOINT_URL}/v2/search", headers=headers, params=parameters
        )
        try:
            data = response.json()["data"][0]
        except IndexError:
            print(f"No flights found for {destination_city_code}.")
            return None

        flight_data = FlightData(
            price=data["price"],
            origin_city=data["route"][0]["cityFrom"],
            origin_airport=data["route"][0]["flyFrom"],
            destination_city=data["route"][0]["cityTo"],
            destination_airport=data["route"][0]["flyTo"],
            out_date=data["route"][0]["local_departure"].split("T")[0],
            return_date=data["route"][1]["local_departure"].split("T")[0],
        )

        print(f"{flight_data.destination_city}: €{flight_data.price}")
        return flight_data
